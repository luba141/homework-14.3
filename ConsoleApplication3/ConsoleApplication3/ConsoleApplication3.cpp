#include <iostream>
std::string myStr = "Marvel\n";
int main()
{
	std::cout << myStr;
	std::cout << "Length: " << myStr.length() << '\n';
	std::cout << "The first letter: " << myStr[0] << '\n';
	std::cout << "The last letter: " << myStr[myStr.length() - 2] << '\n';
	return 0;
}
